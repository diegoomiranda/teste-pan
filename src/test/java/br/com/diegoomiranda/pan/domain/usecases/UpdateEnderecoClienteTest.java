package br.com.diegoomiranda.pan.domain.usecases;

import br.com.diegoomiranda.pan.domain.entities.Cliente;
import br.com.diegoomiranda.pan.domain.entities.Endereco;
import br.com.diegoomiranda.pan.domain.repositories.ClienteRepository;
import br.com.diegoomiranda.pan.usecases.UpdateEnderecoCliente;
import br.com.diegoomiranda.pan.usecases.exceptions.ClienteNotFoundException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.stubbing.Answer;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UpdateEnderecoClienteTest {

    private ClienteRepository repository;

    @Before
    public void setup(){
        this.repository = mock(ClienteRepository.class);
        var notFoundClienteCpf = "93966051826";

        when(repository.getByCpf(notFoundClienteCpf))
                .then((Answer<Optional<Cliente>>) invocationOnMock -> Optional.empty());

        var foundClienteCpf = "68823341884";
        var foundCliente = Cliente.builder()
                .cpf(foundClienteCpf)
                .nome("Cliente")
                .sobrenome("Encontrado")
                .endereco(
                        Endereco.builder()
                                .cep("04088-002")
                                .logradouro("Alameda")
                                .numero(8998L)
                                .cidade("São Paulo")
                                .estado("São Paulo")
                                .build()
                )
                .build();

        when(repository.getByCpf(foundClienteCpf))
                .then((Answer<Optional<Cliente>>) invocationOnMock -> Optional.of(foundCliente));
    }

    @Test
    public void clienteNotFoundShouldShouldThrowException(){
        var findCliente = new UpdateEnderecoCliente(this.repository);

        var notFoundClienteCpf = "93966051826";
        var endereco = Endereco.builder()
                .cep("04088-002")
                .logradouro("Alameda")
                .numero(8998L)
                .cidade("São Paulo")
                .estado("São Paulo")
                .build();

        assertThatThrownBy(() -> findCliente.updateEnderecoCliente(notFoundClienteCpf, endereco))
                .isInstanceOf(ClienteNotFoundException.class);
    }
}
