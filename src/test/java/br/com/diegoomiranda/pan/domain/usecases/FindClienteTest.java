package br.com.diegoomiranda.pan.domain.usecases;

import br.com.diegoomiranda.pan.domain.entities.Cliente;
import br.com.diegoomiranda.pan.domain.entities.Endereco;
import br.com.diegoomiranda.pan.domain.repositories.ClienteRepository;
import br.com.diegoomiranda.pan.usecases.FindCliente;
import org.junit.Test;
import org.mockito.stubbing.Answer;

import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class FindClienteTest {

    @Test
    public void clienteNotFoundShouldReturnEmptyOptional(){
        var repository = mock(ClienteRepository.class);
        var findCliente = new FindCliente(repository);

        var notFoundClienteCpf = "93966051826";

        when(repository.getByCpf(notFoundClienteCpf))
                .then((Answer<Optional<Cliente>>) invocationOnMock -> Optional.empty());

        Optional<Cliente> actualClienteNotFound = findCliente.getByCpf(notFoundClienteCpf);
        assertFalse(actualClienteNotFound.isPresent());
    }

    @Test
    public void clienteFoundShouldReturnRightCliente(){
        var repository = mock(ClienteRepository.class);
        var findCliente = new FindCliente(repository);

        var foundClienteCpf = "68823341884";
        var foundCliente = Cliente.builder()
                .cpf(foundClienteCpf)
                .nome("Cliente")
                .sobrenome("Encontrado")
                .endereco(
                        Endereco.builder()
                                .cep("04088-002")
                                .logradouro("Alameda")
                                .numero(8998L)
                                .cidade("São Paulo")
                                .estado("São Paulo")
                                .build()
                )
                .build();

        when(repository.getByCpf(foundClienteCpf))
                .then((Answer<Optional<Cliente>>) invocationOnMock -> Optional.of(foundCliente));

        Optional<Cliente> actualClienteFound = findCliente.getByCpf(foundClienteCpf);
        assertEquals(Optional.of(foundCliente), actualClienteFound);
    }
}
