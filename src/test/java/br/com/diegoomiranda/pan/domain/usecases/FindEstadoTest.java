package br.com.diegoomiranda.pan.domain.usecases;

import br.com.diegoomiranda.pan.domain.entities.Estado;
import br.com.diegoomiranda.pan.providers.ProviderException;
import br.com.diegoomiranda.pan.providers.EstadoProvider;
import br.com.diegoomiranda.pan.usecases.FindEstado;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class FindEstadoTest {

    private EstadoProvider provider;

    @Before
    public void setup() throws ProviderException {
        this.provider = mock(EstadoProvider.class);
        when(this.provider.getEstados()).thenReturn(
                Arrays.asList(
                        new Estado(88, "Rondônia",  "RO"),
                        new Estado(99, "Minas Gerais","MG"),
                        new Estado(101, "São Paulo","SP"),
                        new Estado(102, "Rio de Janeiro","RJ")
                )
        );
    }

    @Test
    public void listShouldStartWithSPAndRJ() throws ProviderException {
        var findEstados = new FindEstado(this.provider);
        var estados = findEstados.getEstados();
        assertThat(estados.get(0))
                .isEqualTo( new Estado(101, "São Paulo","SP"));
        assertThat(estados.get(1))
                .isEqualTo(new Estado(102, "Rio de Janeiro","RJ"));
    }
}
