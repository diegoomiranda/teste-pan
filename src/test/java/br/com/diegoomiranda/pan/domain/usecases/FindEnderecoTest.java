package br.com.diegoomiranda.pan.domain.usecases;

import br.com.diegoomiranda.pan.domain.entities.Endereco;
import br.com.diegoomiranda.pan.providers.EnderecoProvider;
import br.com.diegoomiranda.pan.usecases.FindEndereco;
import br.com.diegoomiranda.pan.usecases.exceptions.InvalidCepFormatException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.stubbing.Answer;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class FindEnderecoTest {


    private EnderecoProvider provider;

    @Before
    public void setup(){
        this.provider = mock(EnderecoProvider.class);
        when(this.provider.getEnderecoByCep("01310-200"))
                .then((Answer<Optional<Endereco>>) invocationOnMock -> Optional.of(Endereco.builder()
                        .cep("01310-200")
                        .cidade("São Paulo")
                        .logradouro("Avenida Paulista")
                        .bairro("Bela Vista")
                        .build())
        );
        when(this.provider.getEnderecoByCep("00000-000")).then(
                (Answer<Optional<Endereco>>) invocationOnMock -> Optional.empty()
        );

    }

    @Test
    public void invalidCepFormShouldThrowException(){
        var findEndereco = new FindEndereco(this.provider);
        var badFormatCep = "abd789";

        assertThatThrownBy(() -> findEndereco.getEnderecoByCep(badFormatCep));
    }

    @Test
    public void cepNotFoundShouldReturnEmptyOptional() throws InvalidCepFormatException {
        var findEndereco = new FindEndereco(this.provider);
        var cep = "00000-000";

        assertThat(findEndereco.getEnderecoByCep(cep)).isEqualTo(Optional.empty());
    }

    @Test
    public void successShouldReturnEndereco() throws InvalidCepFormatException {
        var findEndereco = new FindEndereco(this.provider);
        var cep = "01310-200";
        var endereco = Optional.of(Endereco.builder()
                .cep(cep)
                .cidade("São Paulo")
                .logradouro("Avenida Paulista")
                .bairro("Bela Vista")
                .build());

        assertThat(findEndereco.getEnderecoByCep(cep)).isEqualTo(endereco);
    }
}
