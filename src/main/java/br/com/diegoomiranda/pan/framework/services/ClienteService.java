package br.com.diegoomiranda.pan.framework.services;

import br.com.diegoomiranda.pan.domain.entities.Cliente;
import br.com.diegoomiranda.pan.domain.entities.Endereco;
import br.com.diegoomiranda.pan.usecases.FindCliente;
import br.com.diegoomiranda.pan.usecases.UpdateEnderecoCliente;
import br.com.diegoomiranda.pan.usecases.exceptions.ClienteNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    private FindCliente findCliente;
    private UpdateEnderecoCliente updateEnderecoCliente;

    public ClienteService(FindCliente findCliente, UpdateEnderecoCliente updateEnderecoCliente) {
        this.findCliente = findCliente;
        this.updateEnderecoCliente = updateEnderecoCliente;
    }

    public Optional<Cliente> getByCpf(String cpf){
        return this.findCliente.getByCpf(cpf);
    }

    public void updateEnderecoCliente(String cpf, Endereco newEndereco) throws ClienteNotFoundException {
        this.updateEnderecoCliente.updateEnderecoCliente(cpf, newEndereco);
    }
}
