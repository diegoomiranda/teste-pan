package br.com.diegoomiranda.pan.framework.repositories;

import br.com.diegoomiranda.pan.domain.entities.Cliente;
import br.com.diegoomiranda.pan.domain.repositories.ClienteRepository;
import br.com.diegoomiranda.pan.framework.datasource.SimpleMapDataSource;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class ClienteRepositoryImpl implements ClienteRepository {

    private SimpleMapDataSource data;

    public ClienteRepositoryImpl(SimpleMapDataSource data) {
        this.data = data;
    }

    @Override
    public Optional<Cliente> getByCpf(String cpf) {
        return  Optional.ofNullable(this.data.get(cpf));
    }

    @Override
    public void save(Cliente cliente) {
        this.data.put(cliente);
    }
}
