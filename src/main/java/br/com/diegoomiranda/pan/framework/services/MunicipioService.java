package br.com.diegoomiranda.pan.framework.services;

import br.com.diegoomiranda.pan.providers.ProviderException;
import br.com.diegoomiranda.pan.usecases.FindMunicipio;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MunicipioService {

    private FindMunicipio findMunicipio;

    public MunicipioService(FindMunicipio findMunicipio) {
        this.findMunicipio = findMunicipio;
    }

    public List<String> getMunicipiosFromEstado(long idEstado) throws ProviderException {
        return this.findMunicipio.getMunicipiosFromEstado(idEstado);
    }
}
