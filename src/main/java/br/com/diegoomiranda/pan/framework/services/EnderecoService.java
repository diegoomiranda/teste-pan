package br.com.diegoomiranda.pan.framework.services;

import br.com.diegoomiranda.pan.domain.entities.Endereco;
import br.com.diegoomiranda.pan.usecases.FindEndereco;
import br.com.diegoomiranda.pan.usecases.exceptions.InvalidCepFormatException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class EnderecoService {

    private FindEndereco findEndereco;

    public EnderecoService(FindEndereco findEndereco) {
        this.findEndereco = findEndereco;
    }

    public Optional<Endereco> getEnderecoByCep(String cep) throws InvalidCepFormatException {
        return this.findEndereco.getEnderecoByCep(cep);
    }
}
