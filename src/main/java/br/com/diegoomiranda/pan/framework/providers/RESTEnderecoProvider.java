package br.com.diegoomiranda.pan.framework.providers;

import br.com.diegoomiranda.pan.domain.entities.Endereco;
import br.com.diegoomiranda.pan.framework.config.ProviderConfiguration;
import br.com.diegoomiranda.pan.providers.EnderecoProvider;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

@Component
public class RESTEnderecoProvider implements EnderecoProvider {

    private ProviderConfiguration config;

    public RESTEnderecoProvider(ProviderConfiguration config) {
        this.config = config;
    }

    @Override
    public Optional<Endereco> getEnderecoByCep(String cep) {
        var rest = new RestTemplate();
        String url = String.format(config.getEnderecoUrl(), cep);
        RestProviderBodyResponse response = rest.getForObject(url, RestProviderBodyResponse.class);

        if(response != null){
             var endereco = Endereco.builder()
                    .bairro(response.bairro)
                    .logradouro(response.logradouro)
                    .cidade(response.localidade)
                    .build();

             return Optional.of(endereco);
        }

        return Optional.empty();
    }


    public static class RestProviderBodyResponse {
        private String cep;
        private String logradouro;
        private String complemento;
        private String bairro;
        private String localidade;

        public String getCep() {
            return cep;
        }

        public void setCep(String cep) {
            this.cep = cep;
        }

        public String getLogradouro() {
            return logradouro;
        }

        public void setLogradouro(String logradouro) {
            this.logradouro = logradouro;
        }

        public String getComplemento() {
            return complemento;
        }

        public void setComplemento(String complemento) {
            this.complemento = complemento;
        }

        public String getBairro() {
            return bairro;
        }

        public void setBairro(String bairro) {
            this.bairro = bairro;
        }

        public String getLocalidade() {
            return localidade;
        }

        public void setLocalidade(String localidade) {
            this.localidade = localidade;
        }
    }

}
