package br.com.diegoomiranda.pan.framework.providers;

import br.com.diegoomiranda.pan.domain.entities.Estado;
import br.com.diegoomiranda.pan.framework.config.ProviderConfiguration;
import br.com.diegoomiranda.pan.providers.EstadoProvider;
import br.com.diegoomiranda.pan.providers.ProviderException;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class RESTEstadoProvider implements EstadoProvider {

    private ProviderConfiguration config;

    public RESTEstadoProvider(ProviderConfiguration config) {
        this.config = config;
    }

    @Override
    public List<Estado> getEstados() throws ProviderException {
        var rest = new RestTemplate();
        String url = config.getEstadoUrl();

        try {
            ResponseEntity<List<RestEstadoProviderBodyResponse>> response =
                    rest.exchange(url, HttpMethod.GET, null, getParameterizedType());
            var estados = response.getBody();

            if(estados != null){
                return estados.stream()
                        .map(o -> new Estado(o.getId(), o.getNome(), o.getSigla()))
                        .collect(Collectors.toList());
            }
        } catch (Exception e){
            throw new ProviderException("Não foi possível recuperar lista de estados", e);
        }

        return Collections.emptyList();
    }

    private ParameterizedTypeReference<List<RestEstadoProviderBodyResponse>> getParameterizedType(){
        return new ParameterizedTypeReference<>() {
        };
    }

    public static class RestEstadoProviderBodyResponse {

        private long id;
        private String sigla;
        private String nome;

        public String getSigla() {
            return sigla;
        }

        public void setSigla(String sigla) {
            this.sigla = sigla;
        }

        public String getNome() {
            return nome;
        }

        public void setNome(String nome) {
            this.nome = nome;
        }

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }
    }
}
