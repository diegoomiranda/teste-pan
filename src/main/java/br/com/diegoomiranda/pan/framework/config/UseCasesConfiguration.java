package br.com.diegoomiranda.pan.framework.config;

import br.com.diegoomiranda.pan.domain.repositories.ClienteRepository;
import br.com.diegoomiranda.pan.providers.EnderecoProvider;
import br.com.diegoomiranda.pan.providers.EstadoProvider;
import br.com.diegoomiranda.pan.providers.MunicipioProvider;
import br.com.diegoomiranda.pan.usecases.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UseCasesConfiguration {

    @Bean
    public FindEndereco getFindEndereco(EnderecoProvider provider){
        return new FindEndereco(provider);
    }

    @Bean
    public FindCliente getFindCliente(ClienteRepository repository){
        return new FindCliente(repository);
    }

    @Bean
    public FindEstado getFindEstado(EstadoProvider provider){
        return new FindEstado(provider);
    }

    @Bean
    public FindMunicipio getFindMunicipio(MunicipioProvider provider){
        return new FindMunicipio(provider);
    }

    @Bean
    public UpdateEnderecoCliente getUpdateClienteEndereco(ClienteRepository repository){
        return new UpdateEnderecoCliente(repository);
    }
}
