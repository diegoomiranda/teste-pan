package br.com.diegoomiranda.pan.framework.datasource;

import br.com.diegoomiranda.pan.domain.entities.Cliente;

import java.util.Map;

public class SimpleMapDataSource {

    private Map<String, Cliente> data;

    public SimpleMapDataSource(Map<String, Cliente> data){
        this.data = data;
    }

    public Cliente get(String cpf){
        return this.data.get(cpf);
    }

    public void put(Cliente cliente){
        this.data.put(cliente.getCpf(), cliente);
    }

    public void setupData(Map<String, Cliente> data){
        this.data = data;
    }
}
