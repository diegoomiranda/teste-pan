package br.com.diegoomiranda.pan.framework.controllers;

import br.com.diegoomiranda.pan.domain.entities.Endereco;
import br.com.diegoomiranda.pan.framework.services.EnderecoService;
import br.com.diegoomiranda.pan.usecases.exceptions.InvalidCepFormatException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/endereco")
public class EnderecoController {

    private EnderecoService service;

    public EnderecoController(EnderecoService service) {
        this.service = service;
    }

    @GetMapping("/cep/{cep}")
    public ResponseEntity<Endereco> getEnderecoByCep(@PathVariable("cep") String cep) throws InvalidCepFormatException {
        var endereco = this.service.getEnderecoByCep(cep);
        return ResponseEntity.of(endereco);
    }

}
