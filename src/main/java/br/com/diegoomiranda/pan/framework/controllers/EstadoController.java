package br.com.diegoomiranda.pan.framework.controllers;

import br.com.diegoomiranda.pan.domain.entities.Estado;
import br.com.diegoomiranda.pan.providers.ProviderException;
import br.com.diegoomiranda.pan.framework.services.EstadoService;
import br.com.diegoomiranda.pan.framework.services.MunicipioService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/estados")
public class EstadoController {

    private EstadoService service;
    private MunicipioService municipioService;

    public EstadoController(EstadoService service,
                            MunicipioService municipioService) {
        this.service = service;
        this.municipioService = municipioService;
    }

    @GetMapping("/")
    public List<Estado> getEstados() throws ProviderException {
        return this.service.getEstados();
    }

    @GetMapping("/estados/{id}/municipios")
    public List<String> getMunicipiosFromEstado(@PathVariable("id") long idEstado) throws ProviderException {
        return this.municipioService.getMunicipiosFromEstado(idEstado);
    }
}
