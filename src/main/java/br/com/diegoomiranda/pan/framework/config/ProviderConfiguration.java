package br.com.diegoomiranda.pan.framework.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("providers")
public class ProviderConfiguration {

    private String estadoUrl;
    private String enderecoUrl;
    private String municipioUrl;

    public String getEstadoUrl() {
        return estadoUrl;
    }

    public String getEnderecoUrl() {
        return enderecoUrl;
    }

    public void setEstadoUrl(String estadoUrl) {
        this.estadoUrl = estadoUrl;
    }

    public void setEnderecoUrl(String enderecoUrl) {
        this.enderecoUrl = enderecoUrl;
    }

    public String getMunicipioUrl() {
        return municipioUrl;
    }

    public void setMunicipioUrl(String municipioUrl) {
        this.municipioUrl = municipioUrl;
    }
}
