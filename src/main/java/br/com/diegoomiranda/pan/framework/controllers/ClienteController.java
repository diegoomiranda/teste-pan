package br.com.diegoomiranda.pan.framework.controllers;

import br.com.diegoomiranda.pan.domain.entities.Cliente;
import br.com.diegoomiranda.pan.domain.entities.Endereco;
import br.com.diegoomiranda.pan.framework.services.ClienteService;
import br.com.diegoomiranda.pan.usecases.exceptions.ClienteNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    private ClienteService service;

    public ClienteController(ClienteService service) {
        this.service = service;
    }

    @GetMapping("/get/{cpf}")
    public ResponseEntity<Cliente> getClienteByCpf(@PathVariable("cpf") String cpf){
        Optional<Cliente> cliente = service.getByCpf(cpf);
        return ResponseEntity.of(cliente);
    }

    @PostMapping("/update/endereco")
    public void updateEnderecoCliente(UpdateEnderecoClienteRequest request) throws ClienteNotFoundException {
        this.service.updateEnderecoCliente(request.cpf,
                Endereco.builder()
                        .cep(request.endereco.cep)
                        .logradouro(request.endereco.logradouro)
                        .numero(request.endereco.numero)
                        .bairro(request.endereco.bairro)
                        .cidade(request.endereco.cidade)
                        .estado(request.endereco.estado)
                        .build());
    }


    public static class EnderecoRequest{
        private String cep;
        private String logradouro;
        private Long numero;
        private String complemento;
        private String cidade;
        private String estado;
        private String bairro;

        public String getCep() {
            return cep;
        }

        public void setCep(String cep) {
            this.cep = cep;
        }

        public String getLogradouro() {
            return logradouro;
        }

        public void setLogradouro(String logradouro) {
            this.logradouro = logradouro;
        }

        public Long getNumero() {
            return numero;
        }

        public void setNumero(Long numero) {
            this.numero = numero;
        }

        public String getComplemento() {
            return complemento;
        }

        public void setComplemento(String complemento) {
            this.complemento = complemento;
        }

        public String getCidade() {
            return cidade;
        }

        public void setCidade(String cidade) {
            this.cidade = cidade;
        }

        public String getEstado() {
            return estado;
        }

        public void setEstado(String estado) {
            this.estado = estado;
        }

        public String getBairro() {
            return bairro;
        }

        public void setBairro(String bairro) {
            this.bairro = bairro;
        }
    }

    public static class UpdateEnderecoClienteRequest{
        private String cpf;
        private EnderecoRequest endereco;

        public String getCpf() {
            return cpf;
        }

        public void setCpf(String cpf) {
            this.cpf = cpf;
        }

        public EnderecoRequest getEndereco() {
            return endereco;
        }

        public void setEndereco(EnderecoRequest endereco) {
            this.endereco = endereco;
        }
    }
}
