package br.com.diegoomiranda.pan.framework.services;

import br.com.diegoomiranda.pan.domain.entities.Estado;
import br.com.diegoomiranda.pan.providers.ProviderException;
import br.com.diegoomiranda.pan.usecases.FindEstado;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EstadoService {

    private FindEstado findEstado;

    public EstadoService(FindEstado findEstado) {
        this.findEstado = findEstado;
    }

    public List<Estado> getEstados() throws ProviderException {
        return this.findEstado.getEstados();
    }
}
