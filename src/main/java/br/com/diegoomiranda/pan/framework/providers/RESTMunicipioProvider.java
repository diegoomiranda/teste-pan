package br.com.diegoomiranda.pan.framework.providers;

import br.com.diegoomiranda.pan.framework.config.ProviderConfiguration;
import br.com.diegoomiranda.pan.providers.MunicipioProvider;
import br.com.diegoomiranda.pan.providers.ProviderException;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class RESTMunicipioProvider implements MunicipioProvider {

    private ProviderConfiguration config;

    public RESTMunicipioProvider(ProviderConfiguration config) {
        this.config = config;
    }

    @Override
    public List<String> getMunicipiosFromEstado(long idEstado) throws ProviderException {
        var rest = new RestTemplate();
        String url = String.format(config.getMunicipioUrl(), String.valueOf(idEstado));

        try {
            ResponseEntity<List<RestMunicipioProviderBodyResponse>> response =
                    rest.exchange(url, HttpMethod.GET, null, getParameterizedType());
            var municipios = response.getBody();

            if(municipios != null){
                return municipios.stream()
                        .map(RestMunicipioProviderBodyResponse::getNome)
                        .collect(Collectors.toList());
            }
        } catch (Exception e){
            throw new ProviderException("Não foi possível recuperar lista de municípios", e);
        }

        return Collections.emptyList();
    }

    private ParameterizedTypeReference<List<RestMunicipioProviderBodyResponse>> getParameterizedType(){
        return new ParameterizedTypeReference<>() {
        };
    }

    public static class RestMunicipioProviderBodyResponse{
        private String nome;

        public String getNome() {
            return nome;
        }

        public void setNome(String nome) {
            this.nome = nome;
        }
    }
}
