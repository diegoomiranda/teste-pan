package br.com.diegoomiranda.pan;

import br.com.diegoomiranda.pan.domain.entities.Cliente;
import br.com.diegoomiranda.pan.domain.entities.Endereco;
import br.com.diegoomiranda.pan.framework.datasource.SimpleMapDataSource;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.HashMap;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public SimpleMapDataSource initInMemoryDatasource(){
        var data = new HashMap<String, Cliente>();
        data.put("03794403266", Cliente.builder()
                        .nome("Huguinho")
                        .sobrenome("Donald")
                        .cpf("03794403266")
                        .endereco(Endereco.builder()
                                .logradouro(" Av. Paulista")
                                .numero(1578L)
                                .bairro("Bela Vista")
                                .cep("01310-200")
                                .cidade("São Paulo")
                                .estado("SP")
                                .build())
                        .build());
        data.put("68912572121", Cliente.builder()
                        .nome("Zézinho")
                        .sobrenome("Donald")
                        .cpf("68912572121")
                        .endereco(Endereco.builder()
                                .logradouro("Av. Europa")
                                .numero(158L)
                                .bairro("Jardim Europa")
                                .cep("01449-000")
                                .cidade("São Paulo")
                                .estado("SP")
                                .build())
                        .build());
        data.put("64667881054", Cliente.builder()
                        .nome("Luizinho")
                        .sobrenome("Donald")
                        .cpf("64667881054")
                        .endereco(Endereco.builder()
                                .logradouro(" Avenida Mercúrio, Parque Dom Pedro II")
                                .numero(158L)
                                .bairro("Brás")
                                .cep("03003-060")
                                .cidade("São Paulo")
                                .estado("SP")
                                .build())
                        .build());

        return new SimpleMapDataSource(data);
    }
}
