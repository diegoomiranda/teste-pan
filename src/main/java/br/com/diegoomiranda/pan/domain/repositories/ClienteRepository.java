package br.com.diegoomiranda.pan.domain.repositories;

import br.com.diegoomiranda.pan.domain.entities.Cliente;

import java.util.Optional;

public interface ClienteRepository {

    Optional<Cliente> getByCpf(String cpf);

    void save(Cliente cliente);

}
