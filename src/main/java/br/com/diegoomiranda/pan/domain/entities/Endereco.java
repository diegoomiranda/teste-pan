package br.com.diegoomiranda.pan.domain.entities;

import java.util.Objects;

public class Endereco {

    private String cep;
    private String logradouro;
    private Long numero;
    private String complemento;
    private String cidade;
    private String estado;
    private String bairro;

    public static Endereco.Builder builder(){
        return new Endereco.Builder();
    }
    public String getCep() {
        return cep;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public String getCidade() {
        return cidade;
    }

    public String getEstado() {
        return estado;
    }

    public Long getNumero() {
        return numero;
    }

    public String getComplemento() {
        return complemento;
    }

    public String getBairro() {
        return bairro;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Endereco endereco = (Endereco) o;
        return Objects.equals(numero, endereco.numero) &&
                Objects.equals(cep, endereco.cep) &&
                Objects.equals(logradouro, endereco.logradouro) &&
                Objects.equals(complemento, endereco.complemento) &&
                Objects.equals(cidade, endereco.cidade) &&
                Objects.equals(estado, endereco.estado) &&
                Objects.equals(bairro, endereco.bairro);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cep, logradouro, numero, complemento, cidade, estado, bairro);
    }

    @Override
    public String toString() {
        return "Endereco{" +
                "cep='" + cep + '\'' +
                ", logradouro='" + logradouro + '\'' +
                ", numero=" + numero +
                ", complemento='" + complemento + '\'' +
                ", cidade='" + cidade + '\'' +
                ", estado='" + estado + '\'' +
                ", bairro='" + bairro + '\'' +
                '}';
    }

    public static final class Builder {
        private String cep;
        private String logradouro;
        private Long numero;
        private String cidade;
        private String estado;
        private String complemento;
        private String bairro;

        private Builder() {
        }

        public Builder with(Endereco endereco){
            this.cidade = endereco.cidade;
            this.cep = endereco.cep;
            this.logradouro = endereco.logradouro;
            this.numero = endereco.numero;
            this.estado = endereco.estado;
            this.complemento = endereco.complemento;
            this.bairro = endereco.bairro;
            return this;
        }

        public Builder cep(String cep) {
            this.cep = cep;
            return this;
        }

        public Builder logradouro(String logradouro) {
            this.logradouro = logradouro;
            return this;
        }

        public Builder numero(Long numero) {
            this.numero = numero;
            return this;
        }

        public Builder cidade(String cidade) {
            this.cidade = cidade;
            return this;
        }

        public Builder estado(String estado) {
            this.estado = estado;
            return this;
        }

        public Builder complemento(String complemento) {
            this.complemento = complemento;
            return this;
        }

        public Builder bairro(String bairro){
            this.bairro = bairro;
            return this;
        }

        public Endereco build() {
            Endereco endereco = new Endereco();
            endereco.estado = this.estado;
            endereco.logradouro = this.logradouro;
            endereco.numero = this.numero;
            endereco.cidade = this.cidade;
            endereco.cep = this.cep;
            endereco.complemento = this.complemento;
            endereco.bairro = this.bairro;
            return endereco;
        }
    }
}
