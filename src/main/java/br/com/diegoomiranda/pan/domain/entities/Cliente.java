package br.com.diegoomiranda.pan.domain.entities;

import java.util.Objects;

public class Cliente {

    private String cpf;
    private String nome;
    private String sobrenome;
    private Endereco endereco;

    public static Cliente.Builder builder(){
        return new Cliente.Builder();
    }

    public String getCpf() {
        return cpf;
    }

    public String getNome() {
        return nome;
    }

    public String getSobrenome() {
        return sobrenome;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cliente cliente = (Cliente) o;
        return Objects.equals(cpf, cliente.cpf);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cpf);
    }

    @Override
    public String toString() {
        return "Cliente{" +
                "cpf=" + cpf +
                ", nome='" + nome + '\'' +
                ", sobrenome='" + sobrenome + '\'' +
                ", endereco=" + endereco +
                '}';
    }


    public static final class Builder {
        private String cpf;
        private String nome;
        private String sobrenome;
        private Endereco endereco;

        private Builder() {
        }

        public Builder with(Cliente cliente){
            this.cpf = cliente.cpf;
            this.nome = cliente.nome;
            this.sobrenome = cliente.sobrenome;
            this.endereco = Endereco.builder().with(cliente.endereco).build();
            return this;
        }

        public Builder cpf(String cpf) {
            this.cpf = cpf;
            return this;
        }

        public Builder nome(String nome) {
            this.nome = nome;
            return this;
        }

        public Builder sobrenome(String sobrenome) {
            this.sobrenome = sobrenome;
            return this;
        }

        public Builder endereco(Endereco endereco) {
            this.endereco = endereco;
            return this;
        }

        public Cliente build() {
            Cliente cliente = new Cliente();
            cliente.sobrenome = this.sobrenome;
            cliente.cpf = this.cpf;
            cliente.nome = this.nome;
            cliente.endereco = this.endereco;
            return cliente;
        }
    }
}
