package br.com.diegoomiranda.pan.providers;

import java.util.List;

public interface MunicipioProvider {

    List<String> getMunicipiosFromEstado(long idEstado) throws ProviderException;
}
