package br.com.diegoomiranda.pan.providers;

import br.com.diegoomiranda.pan.domain.entities.Endereco;

import java.util.Optional;

public interface EnderecoProvider {

    Optional<Endereco> getEnderecoByCep(String cep);
}
