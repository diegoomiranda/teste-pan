package br.com.diegoomiranda.pan.providers;

import br.com.diegoomiranda.pan.domain.entities.Estado;

import java.util.List;

public interface EstadoProvider {

    List<Estado> getEstados() throws ProviderException;
}
