package br.com.diegoomiranda.pan.providers;

public class ProviderException extends Exception {

    public ProviderException(String message, Throwable cause) {
        super(message, cause);
    }
}
