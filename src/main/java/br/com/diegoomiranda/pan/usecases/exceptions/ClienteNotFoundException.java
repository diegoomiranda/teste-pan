package br.com.diegoomiranda.pan.usecases.exceptions;

public class ClienteNotFoundException extends Exception {

    public ClienteNotFoundException(String cpf) {
        super(String.format("Cliente com o cpf %s não encontrado", String.valueOf(cpf)));
    }
}
