package br.com.diegoomiranda.pan.usecases;

import br.com.diegoomiranda.pan.providers.ProviderException;
import br.com.diegoomiranda.pan.providers.MunicipioProvider;

import java.util.List;

public class FindMunicipio {

    private MunicipioProvider provider;

    public FindMunicipio(MunicipioProvider provider) {
        this.provider = provider;
    }

    public List<String> getMunicipiosFromEstado(long idEstado) throws ProviderException {
        return this.provider.getMunicipiosFromEstado(idEstado);
    }
}
