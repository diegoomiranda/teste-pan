package br.com.diegoomiranda.pan.usecases.exceptions;

public class InvalidCepFormatException extends Exception {

    public InvalidCepFormatException(String message) {
        super(message);
    }
}
