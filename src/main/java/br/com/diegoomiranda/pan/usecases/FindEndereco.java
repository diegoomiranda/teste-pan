package br.com.diegoomiranda.pan.usecases;

import br.com.diegoomiranda.pan.domain.entities.Endereco;
import br.com.diegoomiranda.pan.providers.EnderecoProvider;
import br.com.diegoomiranda.pan.usecases.exceptions.InvalidCepFormatException;

import java.util.Optional;

public class FindEndereco {

    private static final String CEP_REGEX = "[0-9]{5}-[\\d]{3}";

    private EnderecoProvider provider;

    public FindEndereco(EnderecoProvider provider) {
        this.provider = provider;
    }

    public Optional<Endereco> getEnderecoByCep(String cep) throws InvalidCepFormatException {
        if(isCepValid(cep)) {
            return  provider.getEnderecoByCep(cep);
        } else {
            throw new InvalidCepFormatException(String.format("CEP %s é inválido", cep));
        }
    }

    private boolean isCepValid(String cep){
        if(cep != null && !cep.trim().isEmpty()){
            return cep.matches(CEP_REGEX);
        }

        return false;
    }

}
