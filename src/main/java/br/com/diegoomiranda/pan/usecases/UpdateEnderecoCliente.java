package br.com.diegoomiranda.pan.usecases;

import br.com.diegoomiranda.pan.domain.entities.Cliente;
import br.com.diegoomiranda.pan.domain.entities.Endereco;
import br.com.diegoomiranda.pan.domain.repositories.ClienteRepository;
import br.com.diegoomiranda.pan.usecases.exceptions.ClienteNotFoundException;

public class UpdateEnderecoCliente {

    private ClienteRepository repository;

    public UpdateEnderecoCliente(ClienteRepository repository) {
        this.repository = repository;
    }

    public void updateEnderecoCliente(String cpf, Endereco newEndereco) throws ClienteNotFoundException {
        var cliente = this.repository.getByCpf(cpf);
        if(cliente.isPresent()){
            var clienteUpdated = Cliente.builder().with(cliente.get()).endereco(newEndereco).build();
            this.repository.save(clienteUpdated);
        } else {
            throw new ClienteNotFoundException(cpf);
        }
    }
}
