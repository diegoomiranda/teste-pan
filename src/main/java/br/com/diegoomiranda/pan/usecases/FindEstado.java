package br.com.diegoomiranda.pan.usecases;

import br.com.diegoomiranda.pan.domain.entities.Estado;
import br.com.diegoomiranda.pan.providers.ProviderException;
import br.com.diegoomiranda.pan.providers.EstadoProvider;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class FindEstado {

    private EstadoProvider provider;

    public FindEstado(EstadoProvider provider) {
        this.provider = provider;
    }

    public List<Estado> getEstados() throws ProviderException {
        var estados = this.provider.getEstados();

        var spAndRj = estados.stream().filter(e -> e.getNome().equals("São Paulo") || e.getNome().equals("Rio de Janeiro"))
                .sorted(Comparator.comparing(Estado::getNome))
                .collect(Collectors.toList());

        var estadosSorted = estados.stream()
                .filter(e -> !e.getNome().equals("São Paulo") && !e.getNome().equals("Rio de Janeiro"))
                .sorted(Comparator.comparing(Estado::getNome))
                .collect(Collectors.toList());

        int newIndex = 0;
        for (int i = spAndRj.size(); i > 0; i--) {
            estadosSorted.add(newIndex++ , spAndRj.get(i-1));
        }

        return estadosSorted;
    }
}
