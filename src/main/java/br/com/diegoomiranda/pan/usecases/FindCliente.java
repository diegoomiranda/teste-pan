package br.com.diegoomiranda.pan.usecases;

import br.com.diegoomiranda.pan.domain.entities.Cliente;
import br.com.diegoomiranda.pan.domain.repositories.ClienteRepository;

import java.util.Optional;

public class FindCliente {

    private ClienteRepository repository;

    public FindCliente(ClienteRepository repository) {
        this.repository = repository;
    }

    public Optional<Cliente> getByCpf(String cpf){
        return repository.getByCpf(cpf);
    }
}
