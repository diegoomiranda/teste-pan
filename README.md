# Teste Desenvolvedor Backend


### Disclaimer
Nunca trabalhei utilizando CLEAN Architeture, mas pelo que pude pesquisar rapidamente,
se trata de um forma de aplicar os conceitos de SOLID, então tentei fazer exatamente isso.
No package framework esta isolado todo o código que esta acoplado ao Spring Boot, tirando o Application.java

Alguns ponto sobre o projeto:

 - O projeto foi feito com Java 11
 - A porta padrão para execução esta configurada como 8081
 - A "documentção" da API esta diponível via Swagger no path /swagger-ui.html
 - Existem 3 clientes que são carregados junto com a aplicação com os seguintes cpfs:
    - 03794403266
    - 68912572121
    - 64667881054
 
Para testar:
```
cd [diretório do projeto]
java -jar target/backend-test-1.0.0.jar
```

